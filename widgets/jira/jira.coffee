###

 Copyright 2018 Motorola Solutions, Inc.
 All Rights Reserved.
 Motorola Solutions Confidential Restricted

###
class Dashing.Jira extends Dashing.Widget

  constructor: ->
    super
    @observe 'meterPercentage', (meterPercentage) ->
      $(@node).find(".meter").val(meterPercentage).trigger('change')

  ready: ->
    meter = $(@node).find(".meter")
    meter.attr("data-bgcolor", meter.css("background-color"))
    meter.attr("data-fgcolor", meter.css("color"))
    meter.knob()
    setInterval(@lastUpdateInterval, 500)
	
  onData: (data) ->
    if data.error
      $(@get('node')).find("div.data").hide()
      $(@get('node')).find("div.error").show()
    else
      $(@get('node')).find("div.error").hide()
      $(@get('node')).find("div.data").show()

  lastUpdateInterval: =>
    lastUpdated = new Date(@get('lastUpdated')*1000)
    today = new Date()
    remaining = 30*60 - parseInt(((today - lastUpdated)/1000),10)
    
    m = Math.floor(remaining / 60)
    s = remaining % 60
    m = @formatTime(m)
    s = @formatTime(s)
    if remaining > 0
    	  @set('nextUpdate',  m + ":" + s )
    	  $(@get('node')).find("span.nextUpdateMin").show()
    else
      #@set('nextUpdate', "0")
    	  @set('nextUpdate', "NOW")
    	  $(@get('node')).find("span.nextUpdateMin").hide()


  formatTime: (i) ->
    if i < 10 then "0" + i else i
