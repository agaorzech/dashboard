###

 Copyright 2018 Motorola Solutions, Inc.
 All Rights Reserved.
 Motorola Solutions Confidential Restricted

###
class Dashing.JenkinsTest extends Dashing.Widget

    onData: (data) ->
        if data.status
            # clear existing "status-*" classes
            #$(@get('node')).attr 'class', (i,c) ->
            #   c.replace /\bstatus-\S+/g, ''
            $(@get('node')).find("div.header").attr 'class', (i,c) ->
                c.replace /\bstatus-\S+/g, ''
            $(@get('node')).find("div.footer-header").attr 'class', (i,c) ->
                c.replace /\bstatus-\S+/g, ''
            # add new class
            $(@get('node')).find("div.header").addClass "status-#{data.status}"
            $(@get('node')).find("div.footer-header").addClass "status-#{data.status}"
			
    ready: ->
        setInterval(@cycleData, 10000)
        $(@node).find("h2.status").hide()
    
    cycleData: =>
        console.log $(@node).find("h2.status").text()
        if  $(@node).find("h3.last-build").is(":visible") & $(@node).find("h2.status").text()!='success'
            $(@node).find("h3.last-build").hide()
            $(@node).find("h2.status").fadeOut().fadeIn()
        else
            $(@node).find("h2.status").hide()
            $(@node).find("h3.last-build").fadeOut().fadeIn()
