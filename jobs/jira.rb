#
# Copyright 2018 Motorola Solutions, Inc.
# All Rights Reserved.
# Motorola Solutions Confidential Restricted
#

############################################
# Script getting current Sprint data from JIRA
############################################

############################################
###### For debug and single run
# require 'dotenv'
 #Dotenv.load('config.env')
 #require 'openssl'
############################################
require 'net/http'
require 'json'
require 'time'

############################################
### CONFIG
############################################

# Configurable jobs.
#		key => view name in html view
# 		:view_id => view id in JIRA (visible in URL)

jira_view_mapping = {
  'jira-1' => { :view_id => 206 },
}

# Custom fields in JIRA mapping

jira_custom_fields_config = {
	'open' => { :id => 1 },
	'closed' => { :id => 6 },
	'in progress' => { :id => 3 },
	'in review' => { :id => 10102 },
	'analysis' => { :id => 10001 },
	'resolved' => { :id => 5 },
	'story_point' => {:id => 'customfield_10108'}
}

# Refresh rate in seconds

jira_data_refresh = '1800s'

############################################
### END CONFIG
############################################

# Getting credentials (default config.env)

JIRA_URI = URI.parse(ENV['JIRA_URI'])
MSI_AUTH = {
	'name' => ENV['MSI_USERNAME'],
	'password' => ENV['MSI_PASSWORD']
}

# create HTTP

def jira_create_http
	jira_http = Net::HTTP.new(JIRA_URI.host, JIRA_URI.port)
	if ('https' == JIRA_URI.scheme)
		jira_http.use_ssl     = true
		jira_http.verify_mode = OpenSSL::SSL::VERIFY_NONE
	end
	return jira_http
end

# create HTTP request for given path

def jira_create_http_request(path)
	jira_request = Net::HTTP::Get.new(JIRA_URI.path + path)
	jira_request.basic_auth(MSI_AUTH['name'], MSI_AUTH['password'])
	return jira_request
end

# View details

def jira_get_view_details(view_id)
	jira_http = jira_create_http
	jira_request = jira_create_http_request("/rest/agile/1.0/board/#{view_id}")
	jira_response = jira_http.request(jira_request)
	jira_view = JSON.parse(jira_response.body)
	if jira_view
		return jira_view
	end
		return false
end

# Active sprint

def jira_get_active_sprint(view_id)
	jira_http = jira_create_http
	jira_request = jira_create_http_request("/rest/agile/1.0/board/#{view_id}/sprint?state=active")
	jira_response = jira_http.request(jira_request)
	jira_sprint = JSON.parse(jira_response.body)
	if jira_sprint['values']
		if jira_sprint['values'][0]
			return jira_sprint['values'][0]
		end
	end
	return false
end

# Issues

def jira_get_issues_from_sprint(view_id, sprint_id,fields="status,issuetype")
	jira_http = jira_create_http
	jira_request = jira_create_http_request("/rest/agile/1.0/board/#{view_id}/sprint/#{sprint_id}/issue?fields=#{fields}")
	jira_response = jira_http.request(jira_request)
	jira_issues = JSON.parse(jira_response.body)
	if jira_issues['issues']
		return jira_issues['issues']
	end
	return false
end

# Calculate open, closed issues

def jira_calculate_issues(issues, json, jira_custom_fields_config)
	if json
		json.each do |issue|
			if issue['fields']
				if issue['fields']['status']
					# sum all fields
					issues['all']['count'] += 1
					if issue['fields'][jira_custom_fields_config['story_point'][:id]] and issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int > 0
						issues['all']['story_point'] += issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int
					end
					#sum open issues
					if issue['fields']['status']['id'].to_i == jira_custom_fields_config['open'][:id].to_i
						issues['open']['count'] += 1
						if issue['fields'][jira_custom_fields_config['story_point'][:id]] and issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int > 0
							issues['open']['story_point'] += issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int
						end
					#sum in progress issues
					elsif issue['fields']['status']['id'].to_i == jira_custom_fields_config['in progress'][:id].to_i or 
							issue['fields']['status']['id'].to_i == jira_custom_fields_config['analysis'][:id].to_i
							issues['in progress']['count'] += 1
							if issue['fields'][jira_custom_fields_config['story_point'][:id]] and issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int > 0
								issues['in progress']['story_point'] += issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int
							end
					# sum in review issues
					elsif issue['fields']['status']['id'].to_i == jira_custom_fields_config['in review'][:id].to_i
							issues['in review']['count'] += 1
							if issue['fields'][jira_custom_fields_config['story_point'][:id]] and issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int > 0
								issues['in review']['story_point'] += issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int
							end
					# sume resolved issues
					elsif issue['fields']['status']['id'].to_i == jira_custom_fields_config['resolved'][:id].to_i
							issues['resolved']['count'] += 1
							if issue['fields'][jira_custom_fields_config['story_point'][:id]] and issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int > 0
								issues['resolved']['story_point'] += issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int
							end
					# sum closed issues
					elsif issue['fields']['status']['id'].to_i == jira_custom_fields_config['closed'][:id].to_i
							issues['closed']['count'] += 1
							if issue['fields'][jira_custom_fields_config['story_point'][:id]] and issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int > 0
								issues['closed']['story_point'] += issue['fields'][jira_custom_fields_config['story_point'][:id]].to_int
							end
					else
						printf "Unknown issue type: id: #{issue['fields']['status']['id']} name: #{issue['fields']['status']['name']} \n"
					end
				end
			end
		end
	end
	return issues
end

# format remaining time

def jira_days_left(time)
	a = (time-Time.now.to_i).to_i
	if a < 0
		return '0 days left' end
    case a
      when 0 then return '0 days left'
      when 1..172000 then return '1 day left' # 86400 = 1 day
      else return  ((a+800)/(60*60*24)).to_i.to_s+' days left'
    end
end


# create scheduler for each job
jira_view_mapping.each do |view, view_id|
	SCHEDULER.every jira_data_refresh, allow_overlapping: false,  :first_in => 15 do |id|
		error = false
		view_name = ""
		sprint_name = ""
		days = ""
		issues = {
			'all' => {'count' => 0, 'story_point' => 0},
			'open' => {'count' => 0, 'story_point' => 0},
			'in progress' => {'count' => 0, 'story_point' => 0},
			'in review' => {'count' => 0, 'story_point' => 0},
			'resolved' => {'count' => 0, 'story_point' => 0},
			'closed' => {'count' => 0, 'story_point' => 0}
		}
		
		view_json = jira_get_view_details(view_id[:view_id])
		if (view_json)
			view_name = view_json['name']
			
			sprint_json = jira_get_active_sprint(view_id[:view_id])
			if (sprint_json)
				sprint_name = sprint_json['name']
				sprint_id = sprint_json['id']

				days = jira_days_left(DateTime.parse(sprint_json['endDate']).to_time.to_i)
				
				issues_json = jira_get_issues_from_sprint(view_id[:view_id],sprint_json['id'],"status,issuetype,#{jira_custom_fields_config['story_point'][:id]}")
				issues=jira_calculate_issues(issues,issues_json,jira_custom_fields_config)
								
				if issues['all']['story_point'] == 0
    					percentage = 0
    					more_info = "No sprint currently in progress"
  				else
    					percentage = ((issues['closed']['story_point'].to_i*100)/issues['all']['story_point'].to_i).to_i
    					more_info = "#{issues['closed']['story_point'].to_i} / #{issues['all']['story_point'].to_i}"
  				end
				
				send_event(view, { 					viewName: view_name,
																	sprintName: sprint_name,
																	remainingDays:  days,
																	issuesAllCount: issues['all']['count'],
																	issuesAllPoints: issues['all']['story_point'],
																	issuesOpenCount: issues['open']['count'],
																	issuesOpenPoints: issues['open']['story_point'],
																	issuesInProgressCount: issues['in progress']['count'],
																	issuesInProgressPoints: issues['in progress']['story_point'],
																	issuesInReviewCount: issues['in review']['count'],
																	issuesInReviewPoints: issues['in review']['story_point'],
																	issuesResolvedCount: issues['resolved']['count'],
																	issuesResolvedPoints: issues['resolved']['story_point'],
																	issuesClosedCount: issues['closed']['count'],
																	issuesClosedPoints: issues['closed']['story_point'],
																	meterPercentage: percentage,
																	meterMin: 0,
																	meterMax: 100,
																	moreInfo: more_info,
																	lastUpdated: Time.now.to_i,
																	error: error
				})					
				
			else
				error  = "No active sprint in Jira"
			end
			
		else
			error  = "Cannot get data from Jira for view #{view_id[:view_id]}"
		end
		
		if error
			send_event(view, { 		viewName: view_name,
													lastUpdated: Time.now.to_i,
													error: error
			})
		end
		
 	end
end

