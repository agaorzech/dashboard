#
# Copyright 2018 Motorola Solutions, Inc.
# All Rights Reserved.
# Motorola Solutions Confidential Restricted
#

############################################
# Script parsing json data from jenkins for selected jobs
############################################

############################################
###### For debug and single run
 #require 'dotenv'
 #Dotenv.load('config.env')
 #require 'openssl'
############################################
require 'net/http'
require 'json'
require 'time'

############################################
### CONFIG
############################################

# Configurable jobs.
#		key => job name in jenkins (visible in url)
# 		:dashboard_id => id of the view in your dashboard
#		:branch => (OPTIONAL) branch name, if you only want to get it
#
#		example {'my-super-jobt' => { :dashboard_id => 'your-view-unique-id', :branch => 'development' }}

jenkins_report_mapping = {
	'configuration-report' => { :dashboard_id => 'jenkins-1' },
	'discovery-report' => { :dashboard_id => 'jenkins-2' },
	'external-api-report' => { :dashboard_id => 'jenkins-3' },
	'legacy-test-utils-report' => { :dashboard_id => 'jenkins-4' },
	'log-exporter-report' => { :dashboard_id => 'jenkins-5' },
	'traces-common-services-report' => { :dashboard_id => 'jenkins-6' },
	'traces-common-services-spring-report' => { :dashboard_id => 'jenkins-7' },
	'traces-common-services-testing-report' => { :dashboard_id => 'jenkins-8' },
	'traces-crypto-report' => { :dashboard_id => 'jenkins-9' },
	'server-report' => { :dashboard_id => 'jenkins-10', :branch => 'development' },
  'elastic-search-api-report' => { :dashboard_id => 'jenkins-11' },
  'airs-collector-report' => { :dashboard_id => 'jenkins-12' },
  'imw-adapter-report' => { :dashboard_id => 'jenkins-13' },
  'ios-db-common-report' => { :dashboard_id => 'jenkins-14' },
  'location-api-report' => { :dashboard_id => 'jenkins-15' },
  'location-cache-report' => { :dashboard_id => 'jenkins-16' },
  'uns-adapter-report' => { :dashboard_id => 'jenkins-17' },
  'uploader-astro-report' => { :dashboard_id => 'jenkins-18' },
  'uploader-common-report' => { :dashboard_id => 'jenkins-19' },
  'uploader-dimetra-report' => { :dashboard_id => 'jenkins-21' },
  'zss-simulator-report' => { :dashboard_id => 'jenkins-22' },
  'server-ui-report' => { :dashboard_id => 'jenkins-23' },
  'traces-internal-api-report' => { :dashboard_id => 'jenkins-24' },
  'web-container-report' => { :dashboard_id => 'jenkins-25' },
  'umr-collector-report' => { :dashboard_id => 'jenkins-26' },
  'umr-bts-enabler-report' => { :dashboard_id => 'jenkins-27' },
}

# Refresh rate in seconds
jenkins_report_refresh = '60s'

############################################
### END CONFIG
############################################

# Getting credentials (default config.env)

JENKINS_URI = URI.parse(ENV['JENKINS_URI'])
MSI_AUTH = {
	'name' => ENV['MSI_USERNAME'],
	'password' => ENV['MSI_PASSWORD']
}

# create HTTP

def jenkins_create_http
	jenkins_http = Net::HTTP.new(JENKINS_URI.host, JENKINS_URI.port)
	if ('https' == JENKINS_URI.scheme)
		jenkins_http.use_ssl     = true
		jenkins_http.verify_mode = OpenSSL::SSL::VERIFY_NONE
	end
	return jenkins_http
end

# create HTTP request for given path

def jenkins_create_http_request(path)
	jenkins_request = Net::HTTP::Get.new(JENKINS_URI.path + path)
	jenkins_request.basic_auth(MSI_AUTH['name'], MSI_AUTH['password'])
	return jenkins_request
end

# create json request depend on branch

def jenkins_get_data(job,branch='')
	jenkins_http = jenkins_create_http
	if branch != ''
		jenkins_request = jenkins_create_http_request("/job/#{job}/job/#{branch}/api/json?tree=name,displayName,color,healthReport[description,score],lastBuild[number,status,timestamp,id,result,duration]")
	else
		jenkins_request = jenkins_create_http_request("/job/#{job}/api/json?tree=name,displayName,color,healthReport[description,score],lastBuild[number,status,timestamp,id,result,duration]")
	end
		jenkins_response = jenkins_http.request(jenkins_request)
		jenkins_data = JSON.parse(jenkins_response.body)
	return jenkins_data
end

# format date for last run

def jenkins_pretty_date(time)
	a = (Time.now-time).to_i
    case a
      when 0 then return 'just now'
      when 1 then  return 'a second ago'
      when 2..59 then return  a.to_s+' seconds ago' 
      when 60..119 then return  'a minute ago' #120 = 2 minutes
      when 120..3540 then return  (a/60).to_i.to_s+' minutes ago'
      when 3541..7100 then return  'an hour ago' # 3600 = 1 hour
      when 7101..82800 then return  ((a+99)/3600).to_i.to_s+' hours ago' 
      when 82801..172000 then return 'a day ago' # 86400 = 1 day
      when 172001..518400 then return  ((a+800)/(60*60*24)).to_i.to_s+' days ago'
      when 518400..1036800 then return 'a week ago'
      else return  ((a+180000)/(60*60*24*7)).to_i.to_s+' weeks ago'
    end
end

# create scheduler for each job

jenkins_report_mapping.each do |report_name, report|
	SCHEDULER.every jenkins_report_refresh, allow_overlapping: false,  :first_in => 0 do
	
		branch = ''
		build_number = ''
		build_date = ''
		build_duration =''
		display_name = ''
		status = ''
		health_score = '0'

		if (report[:branch])
			branch = report[:branch]
		end
		
		dashboard_id = report[:dashboard_id]
		
		# Get all Jenkis reports data
		
		jenkins_data = jenkins_get_data(report_name,branch)

		if jenkins_data['lastBuild']
			if jenkins_data['lastBuild']['number']
				build_number = jenkins_data['lastBuild']['number'] end

			if jenkins_data['lastBuild']['timestamp']
				build_date = jenkins_pretty_date( jenkins_data['lastBuild']['timestamp']/1000 ) end

			if jenkins_data['lastBuild']['duration']
				build_duration = jenkins_data['lastBuild']['duration']/1000
				build_duration = Time.at(build_duration).utc.strftime("%H:%M:%S") end

			if jenkins_data['lastBuild']['result']
				status = jenkins_data['lastBuild']['result'].downcase   end

		end
		
		if jenkins_data['displayName']
			display_name = jenkins_data['displayName'] end
		if jenkins_data['healthReport']
			if jenkins_data['healthReport'][0]['score']
				health_score = jenkins_data['healthReport'][0]['score'] end	end
		# send events to dashboard
				
		send_event(dashboard_id, { 	reportName: display_name,
															buildNumber: build_number,
															buildDate:  build_date,
															buildDuration:  build_duration,
															status: status,
															healthScore: health_score })	

	end
end